﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using Models;

namespace Application
{
    public abstract class DbActions<T>
    {
        private BaseDbContext dbContext = new BaseDbContext();

        public abstract void Add(T entity);
        public abstract List<T> GetAll();
        public abstract T GetById(int? entityId);
        public abstract void Edit(T modifiedEntity);
        public abstract void DeleteById(int? entityId);
    }
}
