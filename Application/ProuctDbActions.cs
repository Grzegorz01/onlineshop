﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using Models;
using System.Data.Entity;
using AutoMapper;

namespace Application
{
    public class ProuctDbActions : DbActions<Product>
    {
        private BaseDbContext dbContext = new BaseDbContext();

        public override void Add(Product entity)
        {
            dbContext.Products.Add(entity);
            dbContext.SaveChanges();
        }

        public override List<Product> GetAll()
        {
            return dbContext.Products.ToList();
        }

        public override Product GetById(int? entityId)
        {
            return dbContext.Products.Find(entityId);
        }

        public override void Edit(Product modifiedEntity)
        {
            //var product = dbContext.Products.Find(modifiedProduct.Id);

            //product.Name = modifiedProduct.Name;
            //product.Price = modifiedProduct.Price;
            //product.IsAvailable = modifiedProduct.IsAvailable;

            dbContext.Entry(modifiedEntity).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public override void DeleteById(int? entityId)
        {
            Product product = dbContext.Products.Find(entityId);
            //Product productx = dbContext.Products.First(x => x.Id == productId);
            dbContext.Products.Remove(product);
            dbContext.Entry(product).State = EntityState.Deleted;
            dbContext.SaveChanges();
        }
    }
}
