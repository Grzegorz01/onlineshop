﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataBase;
using Models;

namespace Application
{
    public class OrderDbActions : DbActions<Order>
    {
        private BaseDbContext dbContext = new BaseDbContext();

        public override void Add(Order entity)
        {
            dbContext.Orders.Add((Order)entity);
            dbContext.SaveChanges();
        }

        public override List<Order> GetAll()
        {
            return dbContext.Orders.ToList();
        }

        public override Order GetById(int? entityId)
        {
            return dbContext.Orders.Find(entityId);
        }

        public override void Edit(Order modifiedEntity)
        {
            dbContext.Entry(modifiedEntity).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public override void DeleteById(int? entityId)
        {
            Order order = dbContext.Orders.Find(entityId);
            //Product productx = dbContext.Products.First(x => x.Id == productId);
            dbContext.Orders.Remove(order);
            dbContext.Entry(order).State = EntityState.Deleted;
            dbContext.SaveChanges();
        }
    }
}
