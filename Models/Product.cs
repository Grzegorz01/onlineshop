﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Product : Entity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public double Price { get; set; }
        public bool IsAvailable { get; set; }
        public string Availibility => IsAvailable ? "Yes" : "No";
    }
}
