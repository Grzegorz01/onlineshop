﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Order : Entity
    {
        public List<Product> ProductList { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }
    }
}
