﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace OnlineShop.Tests
{
    [TestClass]
    public class OrderDbActionsTest
    {
        private static int orderId = 867;

        private static int productId = 11;

        private static readonly Product testProduct = new Product()
        {
            Id = productId,
            Name = "testProduct",
            Price = 9.99,
            IsAvailable = true
        };

        private static readonly Order testOrder = new Order()
        {
            Id = orderId,
            ProductList = new List<Product>(),
            Date = DateTime.Parse("2017/12/01")
        };

        [OneTimeSetUp]
        public void Init()
        {
            testOrder.ProductList.Add(testProduct);
        }

        [SetUp]
        public void Setup()
        {
            var dbContext = new OrderDbActions();
            dbContext.DeleteById(orderId);
        }

        [TestMethod]
        public void AddedOrderShouldBeInDatabase()
        {
            var dbContext = new OrderDbActions();

            var addedOrder = testOrder;
            dbContext.Add(addedOrder);

            var addedOrderInBase = dbContext.GetAll().Exists(o => o == addedOrder);

            Assert.IsTrue(addedOrderInBase);
        }

        [TestMethod]
        public void DatabaseShouldNotBeEmpyIforderAdded()
        {
            var dbContext = new OrderDbActions();

            var addedOrder = testOrder;
            dbContext.Add(addedOrder);

            var allorders = dbContext.GetAll();

            Assert.IsNotNull(allorders);
            Assert.IsNotEmpty(allorders);
        }

        [TestMethod]
        public void NewContextShouldNotContainTestorder()
        {
            var dbContext = new OrderDbActions();

            Assert.IsNull(dbContext.GetById(orderId));
        }

        public void EditedorderShouldDiffer()
        {
            var dbContext = new OrderDbActions();
            dbContext.Add(testOrder);
            var currentorder = testOrder;

            var orderWithChanges = new Order()
            {
                Id = orderId,
            };


            var changedorder = dbContext.GetById(orderId);

        }


        public void ShouldNotContainDeletedorder()
        {
            var dbContext = new OrderDbActions();
            var addedOrder = testOrder;

            dbContext.Add(addedOrder);
            Assert.True(dbContext.GetAll().Exists(p => p == addedOrder));

            dbContext.DeleteById(orderId);

            Assert.IsNull(dbContext.GetById(orderId));

        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            Setup();
        }
    }
}
