﻿using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Application;
using Models;

namespace OnlineShop.Tests
{
    [TestClass]
    public class ProductDbActionsTest
    {
        private static int productId = 867;

        private static readonly Product testProduct = new Product()
        {
            Id = productId,
            Name = "testProduct",
            Price = 9.99,
            IsAvailable = true
        };

        [OneTimeSetUp]
        public void Init()
        {
            
        }

        [SetUp]
        public void Setup()
        {
            var dbContext = new ProuctDbActions();
            dbContext.DeleteById(productId);
        }

        [TestMethod]
        public void AddedProductShouldBeInDatabase()
        {
            var dbContext = new ProuctDbActions();

            var addedProduct = testProduct;
            dbContext.Add(addedProduct);

            var addedProductInBase = dbContext.GetAll().Exists(p => p == addedProduct);

            Assert.IsTrue(addedProductInBase);
        }

        [TestMethod]
        public void DatabaseShouldNotBeEmpyIfProductAdded()
        {
            var dbContext = new ProuctDbActions();

            var addedProduct = new Product()
            {
                Name = "Test Product 1",
                IsAvailable = true,
                Price = 10.99,
                Id = productId
            };
            dbContext.Add(addedProduct);

            var allProducts = dbContext.GetAll();

            Assert.IsNotNull(allProducts);
            Assert.IsNotEmpty(allProducts);
        }

        [TestMethod]
        public void NewContextShouldNotContainTestProduct()
        {
            var dbContext = new ProuctDbActions();

            Assert.IsNull(dbContext.GetById(productId));
        }

        public void EditedProductShouldDiffer()
        {
            var dbContext = new ProuctDbActions();
            dbContext.Add(testProduct);
            var currentProduct = testProduct;

            var productWithChanges = new Product()
            {
                Id = productId,
                Name = "Changed Product",
                Price = 9.99,
                IsAvailable = true
            };


            var changedProduct = dbContext.GetById(productId);

        }


        public void ShouldNotContainDeletedProduct()
        {
           var dbContext = new ProuctDbActions();
            var addedProduct = testProduct;

            dbContext.Add(addedProduct);
            Assert.True(dbContext.GetAll().Exists(p => p == addedProduct));

            dbContext.DeleteById(productId);
            
            Assert.IsNull(dbContext.GetById(productId));

        }

        [OneTimeTearDown]
        public void CleanUp()
        {
            Setup();
        }
    }
}
